const restify = require('restify');
const web3 = require('web3');


function respond(req, res, next) {
	
	res.send('test');
	next();
}

var server = restify.createServer();
server.get('/test', respond);

server.listen(8080, function() {
	console.log('%s listening at %s', server.name, server.url);
});